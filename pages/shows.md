<!--
.. title: Shows
.. slug: shows
.. date: 2018-02-14 16:07:31 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

**Upcoming Shows** 
Owen’s playing fewer shows for the moment while working on [Driftwood Soldier](www.driftwoodsoldier.com)!   
Stay tuned for details..  
  
**Recent Shows**
January 23: [The Twisted Tail](http://www.thetwistedtail.com/about/), Philadelphia PA (OahCP and Driftwood Soldier)  
January 2: [The Twisted Tail](http://www.thetwistedtail.com/about/), Philadelphia PA (OahCP and Driftwood Soldier)  
October 19: [House Show](http://www.jjtiziou.net/jj/about/faq/houseconcert), West Philadelphia w/ Rusty Belle and Nadine LaFond  
August 23: Build On Fundraiser at the Yards Brewery, Philadelphia PA  
July 7: Slavic Women’s Ensemble 2nd Anniversary BBQ email for info  
June 27: House Show, West Philadelphia w/ Pretty William-O and the Bumper Jacksons email for info  
May 18: [Party in the Park](http://www.friendsofclarkpark.org/?p=1803) (Clark Park), West Philadelphia PA  
May 2: [Twisted Tail (Juke Joint)](http://www.thetwistedtail.com/juke_joint.html "http://www.thetwistedtail.com/juke_joint.html"), Philadelphia PA  
April 22: House Show, West Philadelphia w/ Yazzy, Jefferson Hamer and Robert Blake, email for info  
April 18th: [Busboys and Poets](http://www.busboysandpoets.com/) (14th and V), Washington D.C. w/ Crow Jane  
April 6: [Red Hook Bait & Tackle](http://redhookbaitandtackle.com/), Brooklyn NY  
March 30: [The Burrow](https://www.facebook.com/events/471980976189383/?notif_t=plan_user_invited), Boston MA w/Big Fuzzy and Pretty William-O  
March 27: 6 pm on [Yurt Radio](http://yurt.hampshire.edu/Site/index.html), Amherst MA ([Unspun Wool](http://unspunwool.bandcamp.com/))   
March 22: [The Roots Cafe](http://cargocollective.com/rootscafebrooklyn), Brooklyn NY  ([Unspun Wool](http://unspunwool.bandcamp.com/))  
March 21: Margarita-Gras 2013!!! Philadelphia PA ([Unspun Wool](http://unspunwool.bandcamp.com/)) email for info  
February 19: [The Blue Moon Diner](http://www.facebook.com/pages/Blue-Moon-Diner/152942638049813), Charlottesville VA  
February 19: 4:30 pm on “Around this Town” WTJU 91.1 FM,  Charlottesville VA  
February 15: [The Depot at Hillsborough Station](http://hillsboroughdepot.com/), Hillsborough NC w/ Ironing Board Sam  
December 14: The Minstrel Project Open Stage, Morristown NJ  
September 30: [Cheese Meat(s) Beer](http://www.cheesemeatsbeer.com/index.html): [Bellingham Beer Lab](http://www.bellinghambeerlab.com/) Event, Bellingham WA w/Cara Alboucq + Zach Brown and Strange Brew September 29: [The Shakedown](http://shakedownbellingham.com/), Bellingham WA w/Big John Bates and Curse of the Black Tongue May 18: [The Spotty Dog](http://www.thespottydog.com/blog/), Hudson NY w/ Sam Moss   
May 11: [Freddy’s Backroom](http://freddysbar.com/), Brooklyn NY   
May 3: [Gypsy Joynt](http://www.gypsyjoyntcafe.net/), Great Barrington MA  
April 26: House Show, Providence RI (email for details)
  
**Tailwind Tour Dates 2012**  
April 17: [Iota Club & Cafe](http://www.iotaclubandcafe.com/), w/ Sligo Creek Stompers, Arlington VA
April 10: [The Station at Southern Rail](http://www.sr-nc.com/), w/ 23 String Band Carrboro NC
April 9: [The Root Bar](http://www.rootbarnumberone.com/), Asheville NC
April 7: [Drifter’s](http://www.driftersnashville.com/), Nashville TN
April 6: [The MD Loft](http://www.facebook.com/pages/The-MD-LOFT/154715107906138?ref=tn_tnmn), Nashville TN
April 4: [Mollie Fontaine Lounge](http://molliefontainelounge.com/), Memphis TN
March 30: The Jamestown Mercantile, Jamestown CO
March 29: [Mercury Cafe](http://www.mercurycafe.com/), Denver CO
March 28: [Pateros Creek Brewing Co.](http://pateroscreekbrewing.com/pcbc/), Fort Collins CO
March 27: [Coal Creek Coffee](http://www.coalcreekcoffee.com/index.php/locations/), Laramie WY
March 25: [Alchemy Coffee](http://www.alchemycoffee.com/), Salt Lake City UT
March 23: [Naked Lounge](http://www.nakedcoffee.net/musicvenue.html), Sacramento CA
March 21: [Madrone Art Bar](http://madroneartbar.com/), San Francisco CA
March 17: Slim’s Cocktail Bar and Restaurant w/Growler, Streakin Healy’s, Portland OR
March 15: [Conor Byrne Pub](http://conorbyrnepub.com/), Seattle WA
March 14: The [Redlight](http://www.redlightwineandcoffee.com/), Bellingham WA
