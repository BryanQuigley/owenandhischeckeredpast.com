<!--
.. title: Photos/Videos/Reviews
.. slug: photosvideosreviews
.. date: 2018-02-14 16:07:48 UTC-05:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

![](../../playing.jpg)
![](../../audience.jpg)

“Owen and His Checkered Past set the perfect musical tone with mandolin mountain blues: Owen up there with a suitcase kick drum strumming out a tight staccato rhythm singing in a clean back country front porch style reminiscent of Doc Watson or a long lost descendant of the Carter Family. The sound is excellent. Lyrics absolutely 21st Century. Name checking Derrida during the excellent song, “Upstanding Citizen Blues.” Later Roby comes up with a banjo and closes out the set with people on their feet dancing like it was 1899.”  
-What’s Up, January 2012

{{% media url="https://www.youtube.com/watch?v=-2p4J1u6DFY" %}}
{{% media url="https://www.youtube.com/watch?v=wgxXuuA44NQ" %}}
